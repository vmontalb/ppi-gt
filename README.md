# PPI Graph-Tool networks

Multiple PPI networks parsed to [Graph-Tool](https://graph-tool.skewed.de/) object.

![alt text](figs/repo-icon.png "One Gt to controll them all")

[[_TOC_]]

## Description
This repository contains the Protein-Protein Interaction(PPI) networks, in a graph-tool gt object.
The idea is that, anyone, can:
 1) Work offline. Most PPI are hosted online and can not operate without connection.
 2) Network science. Take advantage of graph-tool multiple algorithms implementations to explore novel hypothesis.
 3) Replication. More precise control of the data you are working with. Which interactions were included? why? source? etc
 4) Multi-source integration. With gt objects, you can easly integrate mutiple PPI with non-overlapping information.

This repository also includes the scripts used to parse the different ppi networks from the different sites. \
I also included two newly generated networks:
 - An AND network: a network with the join gene-gene interactions between all the networks
 - An OR network: a network with the conjuntion of gene-gene interactions between all the networks.

## PPI networks
 - MIST = https://fgrtools.hms.harvard.edu/MIST/stats.jsp
 - HIPPIE = http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/information.php
 - STRING = https://string-db.org/
 - GENEMANIA = https://genemania.org/

The gt objects also include some information related to the ppi versions used to construct them.


## Functionalities
A script to generate the PPI network from a set of genes its included.
Within the scripts folder, you can find the script bfs_ppi.py (named after Breadth First Search approach).
It can be called by:

```bash
python3 bfs_ppi.py --geneset=/test/GWAS/AD --depth=0 --ppi=genemania --ppi-path=../ppi-networks --outpath=example/path
```

## PPI comparisons
I have generated a jupyter notebook to compare the different ppi networks. \


## Quality Control
A post-hoc checks of gene-name conversions, edge presence, etc were also performed.
You can check them on the `checklist.txt` file. \
\
Using GWAS gene-lists from Alzheimer and ALS analyses, I computed the PPI results (query-only interactors) using the parsed gt objects and the original PPI networks (using online resources). The reuslts are almost 100% (some minor differences noted in `checklist.txt`) overlapping.

## Notes
The current approach ONLY consider physical interaction data.
The provided scripts can be modified to parse other link-types too. 


## Authors Contact
email: victor [dot] montal [at] protonmail [dot] com \
twitter: @vmbotwin

