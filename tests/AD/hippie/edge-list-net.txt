Source Target SourceGene TargetGene Weight 
0 1 PSEN1 PSEN2 0.87
0 0 PSEN1 PSEN1 0.96
0 3 PSEN1 MAPT 0.55
0 27 PSEN1 APP 0.89
0 2 PSEN1 APOE 0.63
0 19 PSEN1 APH1B 0.75
1 1 PSEN2 PSEN2 0.73
1 27 PSEN2 APP 0.84
1 19 PSEN2 APH1B 0.81
2 3 APOE MAPT 0.89
2 27 APOE APP 0.87
2 2 APOE APOE 0.76
2 16 APOE SORL1 0.72
3 3 MAPT MAPT 0.9
3 8 MAPT BIN1 0.87
3 27 MAPT APP 0.79
5 27 AGRN APP 0.62
7 7 NCK2 NCK2 0.63
7 21 NCK2 ABI3 0.63
8 8 BIN1 BIN1 0.85
8 27 BIN1 APP 0.56
8 17 BIN1 RIN3 0.79
10 10 TNIP1 TNIP1 0.73
11 27 HAVCR2 APP 0.56
12 12 CD2AP CD2AP 0.97
12 14 CD2AP CCDC6 0.63
13 13 CLU CLU 0.86
13 27 CLU APP 0.88
16 27 SORL1 APP 0.89
21 21 ABI3 ABI3 0.63
25 27 LILRB2 APP 0.83
27 27 APP APP 0.9
