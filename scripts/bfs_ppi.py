#!/usr/bin/env python
# bfs_ppi.py

#
# Script to perform Breadth-First Search from a set of genes, with up to 3
# depths, and plot resulting network.
# User can specify PPI network, inclusion type and filter
# Output network will be stored.
# If specified, a SBM will be used to cluster genes based on network
#
# Original Author: Victor Montal
# Date_first: March-06-2022
# Date:

# ---------------------------
# Import Libraries
# ---------------------------
import os
import sys
from os.path import join
from pathlib import Path
import time
import argparse

import pandas as pd
import numpy as np

import graph_tool.all as gt

import pdb

# ---------------------------
# Parser Options
# ---------------------------
HELPTEXT = """

bfs_ppi.py [dev version]

Author:
------
Victor Montal
victor.montal [at] protonmail [dot] com


-----

[1] Breadth-First Search:
    https://en.wikipedia.org/wiki/Breadth-first_search

[2] Stochastic Block Model clustering approach:



"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_genel     = 'Path to .txt file with gene list (one per row).'
    h_ppi       = 'PPI network to operate with. Options: string, hippie, genemania, mist, all (all PPI).'
    h_ppip      = 'Full path where the PPI gt-objects are saved'
    h_bfsdepth  = "Depth of BFS. I.e how many jumps/iterations will be performed"
    h_filt      = 'Threshold [0,1] to filter link weights.'
    h_incl      = 'Logical operation to consider edges. Options: AND (links in all PPI), OR (links in any PPI)'
    h_outpath   = 'Construct the PPI network using graph-tool (boolean)'

    parser.add_argument('--geneset',    dest = 'genelist',  action = 'store',
                                        help = h_genel,     required = True)
    parser.add_argument('--depth',      dest = 'bfsdepth',  action = 'store',
                                        help = h_genel,     required = False,
                                        default = 0, type = int)
    parser.add_argument('--ppi',        dest = 'ppif',      action = 'store',
                                        help = h_ppi,       required = False,
                                        default="hippie")
    parser.add_argument('--ppi-path',   dest = 'ppip',      action = 'store',
                                        help = h_ppip,      required = True)
    parser.add_argument('--filter',     dest = 'filt',      action = 'store',
                                        help = h_filt,      required = False,
                                        default = 0.01, type = float)
    parser.add_argument('--inclusion',  dest = 'incoperand',action = 'store',
                                        help = h_incl,      required = False,
                                        default = None)
    parser.add_argument('--outpath',    dest = 'outpath',   action = 'store',
                                        help = h_outpath,   required = True)


    args = parser.parse_args()


    # Sanitize (input exists, write permisions)
    if not os.path.exists(args.genelist):
        parser.error("Input GeneSet files does not exists")

    ppisoption = ['string','hippie', 'mist', 'genemania','all']
    if args.ppif not in ppisoption:
        parser.error("Incorret PPI (--ppi) option. It has to be "+str(ppisoption))

    if args.filt>1 or args.filt<0:
        parser.error("Incorret link filter (--filter) option. Has to be between 0 and 1")

    if args.incoperand != None:
        if args.ppif != 'all':
            print("You specified a a inclusion criteria, but only one PPI selected.")
            print(f'   Ignoring inclusion criteria...')
    if args.ppif == 'all':
        if (args.incoperand != None) and (args.incoperand != "AND") and (args.incoperand != "OR"):
            parser.error("Incorret inclusion criteria (--inclusion) option. It has to be AND or OR")

    return args

# ---------------------------
# Subfunctions
# ---------------------------
def seconds2min(ctime):
    seconds = ctime % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    strtime = "{hour}h: {min}min: {sec}s".format(hour=hour,min=minutes,sec=round(seconds))

    return strtime

def vect2gene(vert: int, g: gt.Graph):
    """
    Obtain gene name, from PropertyMap("").
    """

    return g.vp.genename[vert]

def edge_exist(geneA: str, geneB: str, g: gt.Graph):
    """
    Check if edge from GeneA->GeneB (or viceversa) exist in graph g
    If edge exist, return value of weight
    """
    verticeA = gt.find_vertex(g, g.vp.genename, geneA)
    verticeB = gt.find_vertex(g, g.vp.genename ,geneB)

    if (len(verticeA) * len(verticeB))  == 0:
        return False
    else:
         verticeA = verticeA[0]
         verticeB = verticeB[0]

    edge1 = g.edge(verticeA,verticeB)
    edge2 = g.edge(verticeB,verticeA)  # Check viceversa too

    if (edge1 is None) and (edge2 is None):
        return False
    else:
        return np.max([g.ep.weight[edge1], g.ep.weight[edge2]])


# ---------------------------
# Main Code
# ---------------------------
def bfs_gene(options):
    """
    1.- Load gene list
    2.- Load PPI gt object
    3.- (If) All PPI loaded, apply inclusion criteria
    4.- Sanitize input gene list
    5.- Apply Edge filter
    6.- Apply BFS with D depth iterations
    7.- Plot results
        7.1.- Save net draw
        7.2 - Save edge-list of genes
    """
    # Parse Parameters
    ingenef = options.genelist

    if options.ppif == "all":
        ppisoption = ['string','hippie','mist','genemania']
    else:
        ppisoption = options.ppif

    outnetv = join(options.outpath,'BFS-ppi_nodes.csv')
    outnete = join(options.outpath,'BFS-ppi_nodes.csv')
    outnetpng = join(options.outpath,'BFS-ppi-ed.png')
    outnongenes = join(options.outpath,'genes-not-found.txt')

    loginfo = """\
+++++++++++++++++++++++++++++++
+ PROTEIN-PROTEIN INTERACTION +
+  PPI Breadth-First Search   +
+++++++++++++++++++++++++++++++

INPUTS
------
    * Input Gene list      : {genel}
    * PPI networks         : {ppisnet}
    * PPI path             : {ppipath}

CONFIG
------
    * Logic operand net    : {logoperand}
    * Filter threshold     : {filt}
    * Depth BFS            : {depth}

OUTPUT
------
    * Network Nodes .csv   : {csvnetv}
    * Network Nodes .csv   : {csvnete}
    * Network .png         : {pngnet}
    * Non-used Genes .txt  : {nongenes}

    """.format(genel=options.genelist, ppisnet=ppisoption,
                ppipath=options.ppip, logoperand=str(options.incoperand),
                filt=str(options.filt), depth=str(options.bfsdepth),
                csvnetv=outnetv, csvnete=outnete,
                pngnet=outnetpng, nongenes=outnongenes)
    print(loginfo)

    # ===================
    # Load DATA
    # ==================
    print(f'> Loading gene list..')
    genelist = pd.read_csv(ingenef, names=['QueryGene'])

    # Load PPI nets
    print(f'> Loading PPI network(s)..')
    dictnetsp = {"string" : join(options.ppip,'string_PPI_graph-tool.gt'),
                 "hippie" : join(options.ppip,'hippie_PPI_graph-tool.gt'),
                 "genemania" : join(options.ppip,'genemania_PPI_graph-tool.gt'),
                 "mist" : join(options.ppip,'mist_PPI_graph-tool.gt')}
    if options.ppif != 'all':
        print(f'   Loading PPI '+options.ppif+' network.')
        ppi = gt.load_graph(dictnetsp[options.ppif])
        print(f'   Network loaded.')
        print(ppi)
    else:
        dictnet = dict()
        for cnet in dictnetsp:
            print(f'   Loading PPI '+cnet+' network.')
            dictnet[cnet] = gt.load_graph(dictnetsp[cnet])

    # ===================
    # Apply inlusion criteria
    # ==================
    # Apply inclusion logic criteria
    if options.ppif == 'all':
        print(f'> Applying inclusion criteria to integrate PPI networks..')
        cstime = time.time()
        if options.incoperand == "AND":
            print(f'   Criteria: AND')
            # Init graph
            ppi = gt.Graph(directed=False)
            epropw = ppi.new_edge_property("double")
            epropdb = ppi.new_edge_property("string")
            vprop = ppi.new_vertex_property("string")
            ppi.edge_properties["weight"] = epropw
            ppi.edge_properties["db"] = epropdb
            ppi.vertex_properties["genename"] = vprop

            # Find net with less edges
            keys = list(dictnet.keys())
            netsizes = [dictnet[kk].num_edges() for kk in keys]
            netmin = np.where(netsizes == np.min(netsizes))[0][0]
            cnet = dictnet[keys[netmin]]

            # Loop over all edges
            for v1,v2 in cnet.iter_edges():
                gene1 = vect2gene(v1,cnet)
                gene2 = vect2gene(v2,cnet)
                cweight = [edge_exist(gene1,gene2,dictnet[nn]) for nn in keys]

                if all(cweight):  # Check not any False in cweigh
                    # Check vertices exist
                    vrtx1 = gt.find_vertex(ppi, ppi.vp.genename, gene1)
                    vrtx2 = gt.find_vertex(ppi, ppi.vp.genename, gene2)
                    if len(vrtx1) == 0:
                        vrtx1 = ppi.add_vertex()
                        ppi.vp.genename[vrtx1] = gene1
                    else:
                        vrtx1 = vrtx1[0]
                    if len(vrtx2) == 0:
                        vrtx2 = ppi.add_vertex()
                        ppi.vp.genename[vrtx2] = gene2
                    else:
                        vrtx2 = vrtx2[0]

                    # Check edge doesnt exist and create
                    if not ppi.edge(vrtx1, vrtx2):
                        e = ppi.add_edge(vrtx1, vrtx2)
                        # EdgeProperty of weight and original ppi net
                        posmax = np.where(cweight == np.max(cweight))[0][0]
                        ppi.ep.weight[e] = cweight[posmax]
                        ppi.ep.db[e] = keys[posmax]
            # print time elapsed
            cetime = time.time()
            timeprint = seconds2min(-cstime+cetime)
            print(f'   Elapsed time generate PPI: '+timeprint)

            # Print info networks
            print(f'  Merged PPI network: ')
            print(ppi)
            print(f'    Num vertices: '+str(ppi.num_vertices()))
            print(f'    Num edges: '+str(ppi.num_edges()))

        else:
            cstime = time.time()
            print(f'   Criteria: OR')
            # Init graph
            ppi = gt.Graph(directed=False)
            epropw = ppi.new_edge_property("double")
            epropdb = ppi.new_edge_property("string")
            vprop = ppi.new_vertex_property("string")
            ppi.edge_properties["weight"] = epropw
            ppi.edge_properties["db"] = epropdb
            ppi.vertex_properties["genename"] = vprop
            # Loop over all nets
            keys = list(dictnet.keys())
            for cppi in keys:
                cnet = dictnet[cppi]
                # Loop over vertices in curr net
                for v1,v2 in cnet.iter_edges():
                    gene1 = vect2gene(v1,cnet)
                    gene2 = vect2gene(v2,cnet)

                    # Get final ppi_edge and current net_edge
                    cedge = edge_exist(gene1,gene2,cnet)
                    ppiedge = edge_exist(gene1,gene2,ppi)

                    # If link not in ppi
                    if not ppiedge:
                        # Check if vertex exist
                        vrtx1 = gt.find_vertex(ppi, ppi.vp.genename, gene1)
                        vrtx2 = gt.find_vertex(ppi, ppi.vp.genename, gene2)
                        if len(vrtx1) == 0:
                            vrtx1 = ppi.add_vertex()
                            ppi.vp.genename[vrtx1] = gene1
                        else:
                            vrtx1 = vrtx1[0]
                        if len(vrtx2) == 0:
                            vrtx2 = ppi.add_vertex()
                            ppi.vp.genename[vrtx2] = gene2
                        else:
                            vrtx2 = vrtx2[0]

                        # Add link
                        new_e = ppi.add_edge(vrtx1, vrtx2)
                        ppi.ep.weight[new_e] = cedge
                        ppi.ep.db[new_e] = cppi
                    # If link aleady in ppi
                    else:
                        # Keep highest weight
                        if ppiedge < cedge:
                            vrtx1 = gt.find_vertex(ppi, ppi.vp.genename, gene1)[0]
                            vrtx2 = gt.find_vertex(ppi, ppi.vp.genename, gene2)[0]
                            curr_edge = ppi.edge(vrtx1,vrtx2)
                            ppi.ep.weight[curr_edge] = cedge
                            ppi.ep.db[curr_edge] = cppi
            # print time elapsed
            cetime = time.time()
            timeprint = seconds2min(-cstime+cetime)
            print(f'   Elapsed time generate PPI: '+timeprint)

            # Print info networks
            print(f'  Merged PPI network: ')
            print(ppi)
            print(f'    Num vertices: '+str(ppi.num_vertices()))
            print(f'    Num edges: '+str(ppi.num_edges()))

    # ===================
    # Apply edge filters (if)
    # ==================
    if options.filt != 0.01:
        print(f'\n\n > Filtering edges with weight threshold of: '+str(options.filt))
        epropw = ppi.new_edge_property("boolean")
        ppi.ep.filter = epropw
        for cedge in ppi.edges():
            if ppi.ep.weight[cedge] > options.filt:
                ppi.ep.filter[cedge] = True
            else:
                ppi.ep.filter[cedge] = False
        ppi.set_edge_filter(ppi.ep.filter)
        ppi.purge_edges()

        print(f'  After filtering PPI network: ')
        print(ppi)
        print(f'    Num vertices: '+str(ppi.num_vertices()))
        print(f'    Num edges: '+str(ppi.num_edges()))

    # ===================
    # Sanitize input genes exist in PPI
    # ==================
    print(f'\n\n > Sanitize input query genes ')
    querygenestmp = genelist['QueryGene'].to_list()
    genesppi = [ ppi.vp.genename[vv] for vv in ppi.iter_vertices() ]

    querygenes = [cgene for cgene in querygenestmp if cgene in genesppi]
    nongenes = [cgene for cgene in querygenestmp if cgene not in querygenes]

    with open(outnongenes,'w') as f:
        for nongene in nongenes:
            f.write(nongene+'\n')

    # ===================
    # Apply BFS with D depth iterations
    # ==================
    print(f'\n\n > Computing Breadth-First Search ')
    print(f'   - Depth: '+str(options.bfsdepth))
    print(f'   - Query Genes: '+str(len(querygenes)))

    # - PreDefine output graph
    outppi = gt.Graph(directed=False)
    epropdb = outppi.new_edge_property("double")
    vpropname = outppi.new_vertex_property("string")
    vproptype = outppi.new_vertex_property("float")
    outppi.edge_properties["weight"] = epropdb
    outppi.vertex_properties["genename"] = vpropname
    outppi.vertex_properties["vtype"] = vproptype

    # - Add query nodes into the final network
    for cgene in querygenes:
        v = outppi.add_vertex()
        outppi.vp.genename[v] = cgene
        outppi.vp.vtype[v] = 1.

    # - Iterate N-depth search
    if options.bfsdepth == 0:
        # - Keep only links between query proteins
        for cgene in querygenes:
            # - Get vertex ID from gene in output PPI
            cvtxout = gt.find_vertex(outppi, outppi.vp.genename, cgene)[0]
            # - Get vertex ID from gene in input PPI
            cvtxppi = gt.find_vertex(ppi, ppi.vp.genename, cgene)[0]
            # - Get all neighbours from gene in original PPI
            neigh = [vect2gene(vv,ppi) for vv in ppi.get_all_neighbors(cvtxppi)]
            # - Only get genes in query
            for cneigh in neigh:
                if cneigh in querygenes:
                    # - Get neighbour vertex id in original and output PPI
                    neighvtx = gt.find_vertex(outppi, outppi.vp.genename, cneigh)[0]
                    neighvtxppi = gt.find_vertex(ppi, ppi.vp.genename, cneigh)[0]
                    # - Check if edge exists
                    if not outppi.edge(cvtxout,neighvtx):
                        # - Add edge
                        newe = outppi.add_edge(cvtxout,neighvtx)
                        # - Update weight
                        ppie = ppi.edge(cvtxppi,neighvtxppi)
                        outppi.ep.weight[newe] =ppi.ep.weight[ppie]
                    else:
                        # - Find already existing edge
                        newe = outppi.edge(cvtxout,neighvtx)
                        # - Update weight
                        ppie = ppi.edge(cvtxppi,neighvtxppi)
                        outppi.ep.weight[newe] = np.max([ppi.ep.weight[ppie],
                                                        outppi.ep.weight[newe]])

    else:
        # Multiple jump steps
        alladded = querygenes
        geneiterate = alladded.copy()
        for cdepth in range(0,options.bfsdepth):
            print(f'     > Working with depth '+str(cdepth+1))
            nextiteration = list()
            # - Loop over all nodes
            for cgene in geneiterate:
                # - Get vertex ID from gene in output PPI
                cvtxout = gt.find_vertex(outppi, outppi.vp.genename, cgene)[0]
                # - Get vertex ID from gene in input PPI
                cvtxppi = gt.find_vertex(ppi, ppi.vp.genename, cgene)[0]
                # - Get all neighbours from gene in original PPI
                neigh = [vect2gene(vv,ppi) for vv in ppi.get_all_neighbors(cvtxppi)]

                # - Loop over all (orig PPI) neighbours from current gene
                for cneigh in neigh:
                    # Check gene not in output network
                    if cneigh not in alladded:
                        # - Append gene to list of added genes (global and dict)
                        alladded.append(cneigh)
                        nextiteration.append(cneigh)
                        # - Add current neighbour to output PPI
                        v = outppi.add_vertex()
                        outppi.vp.genename[v] = cneigh
                        outppi.vp.vtype[v] = 0.2
                    else:
                        v = gt.find_vertex(outppi, outppi.vp.genename, cneigh)[0]

                    # - Check edge not in netwok
                    if not outppi.edge(cvtxout,v):
                        # - Add edge
                        newe = outppi.add_edge(cvtxout,v)
                        # - Find vertex neighbour in original PPI
                        neighvtxppi = gt.find_vertex(ppi, ppi.vp.genename, cneigh)[0]
                        ppie = ppi.edge(cvtxppi,neighvtxppi)
                        # - Add edge weight to output PPI
                        outppi.ep.weight[newe] =ppi.ep.weight[ppie]

            # Iterate to new set of genes (after BFS)
            geneiterate = nextiteration

    # ===================
    # Plot data
    # ==================
    print(f'\n\n > Plot network.. ')

    # - Compute node degree
    outppi.vp.cdegree = outppi.degree_property_map("total")
    deg = outppi.vp.cdegree.copy()

    # - Generate graph position
    pos = gt.sfdp_layout(outppi, vweight=deg)
    #pos = gt.arf_layout(outppi, max_iter=0)

    # - Plot graph
    tosave = join(options.outpath, 'query-genes-ppi.pdf')
    deg.a = 4 * (np.sqrt(deg.a) * 0.5 + 0.4)

    ebet = gt.betweenness(outppi)[1] # Edge betweeness
    ebet.a /= ebet.a.max() / 10.   # Norm edge betweeness
    eorder = ebet.copy()
    eorder.a *= -1
    pos = gt.sfdp_layout(outppi)  # compute pos
    control = outppi.new_edge_property("vector<double>")
    # Fancy edge position
    for e in outppi.edges():
        d = np.sqrt(sum((pos[e.source()].a - pos[e.target()].a) ** 2)) / 5
        control[e] = [0.3, d, 0.7, d]
    # Define which nodes with text
    disptext = outppi.new_vertex_property("string")
    for v in outppi.vertices():
        if deg[v] > 5 or outppi.vp.vtype[v] == 1.:
            disptext[v] = outppi.vp.genename[v]
        else:
            disptext[v] = ""


    gt.graph_draw(outppi, pos=pos, vertex_fill_color=deg, vorder=deg,
                  vertex_text = disptext, vertex_text_position = -0.1,
                  vertex_font_size = 12,
                  edge_control_points=control, # some curvy edges
                  output_size=(1600, 1600), output=tosave)

    # ===================
    # Store output for Cytoscape or Gephi
    # ==================
    print(f'\n\n > Save network (vertex properties and edge-list file).. ')
    vertextosave = join(options.outpath, 'vertex-properties.txt')
    with open(vertextosave,'w') as f:
        f.write('Vertex Genename \n')
        for vv in outppi.vertices():
            cvtx = str(vv)
            cname = outppi.vp.genename[vv]
            ctype = outppi.vp.vtype[vv]
            line = cvtx+' '+cname+'\n'
            f.write(line)

    edgetosave = join(options.outpath, 'edge-list-net.txt')
    with open(edgetosave,'w') as f:
        f.write('Source Target SourceGene TargetGene Weight \n')
        for ee in outppi.edges():
            csource = str(ee.source())
            ctarget = str(ee.target())
            cgsource = vect2gene(csource,outppi)
            cgtarget = vect2gene(ctarget,outppi)
            cweight = str(outppi.ep.weight[ee])
            line = csource+' '+ctarget+' '+cgsource+' '+cgtarget+' '+cweight+'\n'
            f.write(line)

# ---------------------------
# Run Code
# ---------------------------
if __name__ == "__main__":
    options = options_parser()
    bfs_gene(options)
